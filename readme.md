# Piano Website

![image](https://media1.giphy.com/media/ihhUVdrbwssaA/giphy.gif)

This is my personnal piano website. It is a simple yet sober and elegant bootstrap website to share my piano journey with other music enthusiasts, as well as learning a thing or two in cloud and web development along the way.  
It is hosted on [www.plenitude-ai.fr](http://www.plenitude-ai.fr) with GoDaddy and AWS S3, as a static website in a publicly-open bucket. Videos are hosted on YouTube, as it's free.

Thanks for checking out my work !